// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 2.15.2
let package = Package(
    name: "FreestarAds-Nimbus",
     platforms: [
        .iOS(.v12),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-Nimbus",
            targets: [
                "FreestarAds-Nimbus",
                "FreestarAds-Nimbus-Core",
                "GoogleInteractiveMediaAds",
                "NimbusKit",
                "NimbusCoreKit",
                "NimbusRequestKit",
                "NimbusRenderKit",
                "NimbusRenderStaticKit",
                "NimbusRenderVideoKit",
                "NimbusRenderOMKit",
                "OMSDK_Adsbynimbus",
             ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-core.git",
            from: "5.30.0"
            )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "FreestarAds-Nimbus",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/FreestarAds-Nimbus.xcframework.zip",
            checksum: "11aa7f2be986718ce1638e0e195bc77b8120e299db86cf952fd3dd89fd6dceed"
            ),
        .target(
            name: "FreestarAds-Nimbus-Core",
                dependencies: [
                    .product(name: "FreestarAds", package: "spm-freestarads-core")
                ]
            ),
        .binaryTarget(
            name: "GoogleInteractiveMediaAds",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/GoogleInteractiveMediaAds.xcframework.zip",
            checksum: "ce2c1532ea862814f3cb01a1f0565c7ecfceff9020c38f57796b7ab2d7c59c04"
            ),
        .binaryTarget(
            name: "NimbusKit",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/NimbusKit.xcframework.zip",
            checksum: "44176c24a608f2c4126a6c416df95b2fd8e267414b002b08ce0806966dd817d4"
            ),
        .binaryTarget(
            name: "NimbusCoreKit",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/NimbusCoreKit.xcframework.zip",
            checksum: "77d927247809695261946eae08f7bbc425f2dc79949b7af82af86fee595f10be"
            ),
        .binaryTarget(
            name: "NimbusRequestKit",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/NimbusRequestKit.xcframework.zip",
            checksum: "1603004f0c42cf209e1a502b7fed0ae92e0b6d93fb9d1778a5c3d273bed0cf54"
            ),
        .binaryTarget(
            name: "NimbusRenderKit",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/NimbusRenderKit.xcframework.zip",
            checksum: "8c8600c9e9a8988e8e20e1e20e1cf319fd52247314cd3226f264e5ce58035b49"
            ),
        .binaryTarget(
            name: "NimbusRenderExtensionsKit",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/NimbusRenderExtensionsKit.xcframework.zip",
            checksum: "63aa7590635fd074055078beb82111eb21559f770ce3139ab1ab7ff34c641bf5"
            ),
        .binaryTarget(
            name: "NimbusRenderStaticKit",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/NimbusRenderStaticKit.xcframework.zip",
            checksum: "5d5bd01de648efe2332c026d923362734be351ad8618d0e3ab9cc7116f857d0a"
            ),
        .binaryTarget(
            name: "NimbusRenderVideoKit",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/NimbusRenderVideoKit.xcframework.zip",
            checksum: "4732ab678a51d0534ba9fc629a3942bed37547bfc91e4b84dc0c991ebb3a00d7"
            ),
        .binaryTarget(
            name: "NimbusRenderOMKit",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/NimbusRenderOMKit.xcframework.zip",
            checksum: "772983843d15cfe4676b94e0dba7581fa4199c5d3bc53e741c4d3457ecd18797"
            ),
        .binaryTarget(
            name: "OMSDK_Adsbynimbus",
            url: "https://gitlab.com/freestar/spm-freestarads-nimbus/-/raw/2.15.2/OMSDK_Adsbynimbus.xcframework.zip",
            checksum: "99717b9021e34a1facfebbd11a90c77b29989622dc24002797717a887c8902a4"
            )
    ]
)
